from django.http import JsonResponse
from .models import Attendee
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.filter(conference_id=conference_id)
    return JsonResponse(
        {"attendees": attendees},
        encoder=AttendeeListEncoder, safe=False
    )
#     """
#     Lists the attendees names and the link to the attendee
#     for the specified conference id.

#     Returns a dictionary with a single key "attendees" which
#     is a list of attendee names and URLS. Each entry in the list
#     is a dictionary that contains the name of the attendee and
#     the link to the attendee's information.
#     """

#     response = []
#     attendees = Attendee.objects.all()
#     for attendee in attendees:
#         response.append(
#             {
#                 "name": attendee.name,
#                 "href": attendee.get_api_url(),
#             }
#         )
#     return JsonResponse({"attendees": response})


def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder, safe=False
    )
