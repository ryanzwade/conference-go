from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
import requests
from .acls import get_weather_data

# from .acls import get_photo


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location"
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

# def api_list_conferences(request):
#     """
#     Lists the conference names and the link to the conference.

#     Returns a dictionary with a single key "conferences" which
#     is a list of conference names and URLS. Each entry in the list
#     is a dictionary that contains the name of the conference and
#     the link to the conference's information.

#     {
#         "conferences": [
#             {
#                 "name": conference's name,
#                 "href": URL to the conference,
#             },
#             ...
#         ]
#     }
#     """
#     response = []
#     conferences = Conference.objects.all()
#     for conference in conferences:
#         response.append(
#             {
#                 "name": conference.name,
#                 "href": conference.get_api_url(),
#             }
#         )
#     return JsonResponse({"conferences": response})

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Location object and put it in the content dict
    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )

    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )

    # conference = Conference.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)

        weather = get_weather_data(
            conference.location.city, conference.location.state
        )

        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted ": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        location = Conference.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted ": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)

    try:
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )

# <<<<<<< HEAD
#     response = requests.get(url=f'https://api.pexels.com/v1/search?query={content["city"]}', headers={
#         "Authorization": "TylOEEiLKLPEFZNYJkJ3YI6YY3kQrgQn4cYpWgZj1XlT7qVI46Ainnx9"
#     })
#     print(response.json()["photo"][0]["src"]["original"])

#     # content['picture_url'] = get_city_photo(content['city'])
# =======
#     content['image_url'] = get_photo(content['city'])


    location = Location.objects.create(**content)
    return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.
     """

    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #     )

    # return JsonResponse({"locations": response})

    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.
    """
    # location = Location.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "created": location.created,
    #         "updated": location.updated,
    #         "state": str(location.state),
    #     }
    # )


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }
